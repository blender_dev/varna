$(document).ready(function () {

  let resizeHandler = function () {
    const width = window.innerWidth;
    const products = document.querySelectorAll('.js-bg-selection');
    let url = '';
    for (let i = 0; i < products.length; i++) {
      if (width < 667) {
        url = products[i].getAttribute('data-mobile-bg');
      } else {
        url = products[i].getAttribute('data-main-bg');
      }
      products[i].style.backgroundImage = 'url("' + url + '")';
    }
  };
  window.onresize = function (event) {
    resizeHandler()
  };
  window.onload = function () {
    resizeHandler();
  }

  // Menu

  let $menu = $('.menu');
  let $body = $('html');

  function hideMenu() {
    $menu.removeClass('active');
    $body.removeClass('off');
  }

  $(document).keyup(function (e) {
    if (e.keyCode == 27) {
      hideMenu();
    }
  })

  $('.header__toggle, .menu-toggle-mobile').on('click', function () {
    $menu.addClass('active');
    $body.addClass('off');
  })

  $('.menu__button').on('click', function () {
    hideMenu();
  })

  // Input mask
  $('input[type = "tel"]').inputmask({
    "mask": "+38 (999) 999-99-99"
  });

  $(".header__top .ic__search").click(function () {
    $(".header__top .wrap_search, .input").toggleClass("active");
    $(".header__top .wrap_search input[type='text']").focus();
  });

  $(".static .ic__search").click(function () {
    $(".static .wrap_search, .input").toggleClass("active");
    $(".static .wrap_search input[type='text']").focus();
  });

  $(".filter-toggle").click(function () {
    $(".filter-mobile-block").toggleClass("active ");
  });

  // Gallery slider
  var swiper = new Swiper('.header__slider', {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
    },
    a11y: true,
    keyboardControl: true,
    grabCursor: true,
  });

  var swiper1 = new Swiper('.news__slider', {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
    },
    a11y: true,
    keyboardControl: true,
    grabCursor: true,

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

});

$('.objects__slider').each(function (el) {
  var controlWrapper = $(this).siblings('.another-block'),
    next = controlWrapper.find('.swiper-button-next'),
    prev = controlWrapper.find('.swiper-button-prev'),
    pagination = controlWrapper.find('.swiper-pagination');

  var mySwiper = new Swiper(this, {
    loop: true,
    preloadImages: false,
    lazy: true,
    pagination: {
      el: pagination,
      clickable: true,
    },
    navigation: {
      nextEl: next,
      prevEl: prev,
    }
  })
});

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() >= 80) {
      $('.static').addClass('sticky-menu ');
      $('.jquery-accordion-menu').addClass('d-none ');
    } else {
      $('.static').removeClass('sticky-menu ');
      $('.jquery-accordion-menu').removeClass('d-none ');
    }
  });
});

$('select#region').selectize();

$('#mapsvg area').on('mouseover', function (event) {
  event.stopPropagation();
  $('.reg_img').css('visibility', 'hidden');
  $('#ID_' + $(this).attr('id')).css('visibility', 'visible');
  $('.contact_reg').css('display', 'none');
  $('#short-region-info #' + $(this).attr('id')).css('display', 'block');
});

$('.video__item').magnificPopup({
  disableOn: 700,
  type: 'iframe',
  mainClass: 'mfp-fade',
  removalDelay: 160,
  preloader: false,
  fixedContentPos: false
});

$('#login').on('hidden.bs.modal', function (e) {
  $('#login').modal('hide')
})

$('.history__wrap .item').on('click', function (event) {
  event.stopPropagation();
  $(this).addClass('active');
  $('.text__history').css('display', 'none');
  $('#history__item--content #' + $(this).attr('id')).css('display', 'block');
});